/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.excepciones;

import java.io.FileInputStream;
import java.io.IOException;

/**
 *
 * @author james
 */
public class CapturaExcepcion {

    public void capturaExcepcion() {
        String cadena = null;
        Integer entero = null;
        try {
            System.out.println("Antes de que se genere la excepción.");
            entero = Integer.parseInt(cadena);
            System.out.println("Despues de que se genere la excepción.");
            entero++;
        } catch (RuntimeException exc) {
            System.out.println("Excepcion!  " + exc);
            System.out.println("Mensaje!  " + exc.getMessage());
            System.out.println("Mensaje Localizado!  " + exc.getLocalizedMessage());
            System.out.println("Pila de error!  ");
//            exc.printStackTrace(System.err);
        }
        System.out.println("Una linea final " + entero);
    }

    public void capturaFinally() {
        String cadena = "N/D";
        Integer entero = null;
        try {
            System.out.println("Antes de que se genere la excepción.");
            entero = Integer.parseInt(cadena);
            entero ++;
            System.out.println("No genero excepcion " + entero);
        } catch (RuntimeException exc) {
            System.out.println("Error en tiempo!  " + exc);
        } finally { ///se creo para asegurar el cierre de recursos
            entero = -1;
            System.out.println("Llegamos al finally aqui se puede hacer alguna nueva operacion " + entero);
        }
        entero ++;
        System.out.println("Que paso al final " + entero);
    }

    public void capturaDoble() {
        String maximo = "A";

        for (int i = -2; i < 3; i++) {
            try {
                var resultado = Integer.parseInt(maximo) / i;
                System.out.println("Division(" + maximo + "/" + i + ") =>  " + resultado);

            } catch (NumberFormatException exc) {
                System.out.println("Error de formato al transformar numero " + maximo);
            } catch (ArithmeticException exc) {
                System.out.println("Error no se puede dividir para cero " );
            } finally {
                maximo = "2";
            }
        }
    }

    public void capturaMultiple() {
        String maximo = "A";

        for (int i = -2; i < 3; i++) {
            try {
                var resultado = Integer.parseInt(maximo) / i;
                System.out.println("Division(" + maximo + "/" + i + ") =>  " + resultado);
            } catch (NumberFormatException | ArithmeticException exc) {
                System.out.println("Que paso aqui? " + exc);
            } finally {
                maximo = "2";
            }
        }
    }

    public void capturaAnidada() {
        String maximo = "A";
        try {
            for (int i = -2; i < 3; i++) {
                try {
                    var resultado = Integer.parseInt(maximo) / i;
                    System.out.println("Division(" + maximo + "/" + i + ") =>  " + resultado);
                } catch (NumberFormatException exc) {
                    System.out.println("Error de formato al transformar numero " + maximo);
                } finally {
                    maximo = "2";
                }
            }
        } catch (ArithmeticException exc) {
            System.out.println("Error, no se puede dividir para cero salir");
        }

    }

    public void lanzarExcepcion() {
        String maximo = "A";
        for (int i = -2; i < 3; i++) {
            try {
                if(i == 0 ){
                    System.out.println("Divisor es cero  lanzar excepcion");
                    throw new ArithmeticException("Error no se puede dividir para cero");
                }
                var resultado = Integer.parseInt(maximo) / i;                
                System.out.println("Division(" + maximo + "/" + i + ") =>  " + resultado);
                
            } catch (NumberFormatException exc) {
                System.out.println("Error de formato al transformar numero " + maximo);
            } finally {
                maximo = "2";
            }
        }
    }
    
     public void relanzarExcepcion() {
        String maximo = "A";

        for (int i = -2; i < 3; i++) {
            try {
                var resultado = Integer.parseInt(maximo) / i;                
                System.out.println("Division(" + maximo + "/" + i + ") =>  " + resultado);                
            } catch (NumberFormatException exc) {
                System.out.println("Error de formato al transformar numero " + maximo);
            } catch (Exception exc) {
                throw new RuntimeException("Error division para cero, relanzar como Runtime");
            } finally {
                maximo = "2";
            }
        }
    }

    public void excepcionPersonalizada() throws ExcepcionPueba {
        String maximo = "A";
        for (int i = -2; i < 3; i++) {
            try {
                var resultado = Integer.parseInt(maximo) / i;                
                System.out.println("Division(" + maximo + "/" + i + ") =>  " + resultado);                
            } catch (NumberFormatException exc) {
                System.out.println("Error de formato al transformar numero " + maximo);
            } catch (ArithmeticException exc) {
                throw new ExcepcionPueba(exc, "Error en division para cero", "Denominador");
            } finally {
                maximo = "2";
            }
        }

        System.out.println("OK se ejecuto correctamente");
    }

}
