/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.excepciones;

/**
 *
 * @author james
 */
public class ExcepcionPueba extends Exception{

    String valor;
    
    public ExcepcionPueba(Throwable cause, String mensaje, String valor) {
        super(mensaje, cause);
        this.valor = valor;
        for(StackTraceElement e:cause.getStackTrace()){
            if(e.toString().contains("java:")){
                System.out.println("Contiene");
                this.valor +=  "\n Linea: " + e.toString();
            }
        }
    }

    @Override
    public String getMessage() {
        return "Error en excepcion personalizada " + super.getMessage() + " => Valor error  " + valor;
    }
    
    
    
}
