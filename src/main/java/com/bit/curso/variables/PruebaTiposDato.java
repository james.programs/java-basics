/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.variables;

/**
 *
 * @author james
 */
public class PruebaTiposDato {
    

    /**
     * Prueba de tipos de datos
     */
    public void tipos() {
        int enteroPrimitivo;
        Integer enteroEnvoltorio = 10;
        System.out.println("Rango Entero: " + Integer.MIN_VALUE + " - " + Integer.MAX_VALUE);
        enteroPrimitivo = enteroEnvoltorio.intValue(); 
        System.out.println("Asignacion y recuperacion  " + enteroPrimitivo  + " => " + enteroEnvoltorio);
        double doublePrimitivo = 10;
        Double doubleEnvoltorio = 10.0;
        System.out.println("Doubles " + doublePrimitivo + " => " + doubleEnvoltorio);
        char charPrimitivo = 'A';
        Character charEnvoltorio = 65;
        System.out.println("Chars: " + charPrimitivo + " - " + charEnvoltorio);
        Object props = System.getProperties();
        System.out.println("En java todo es Objecto y todo objeto puede ser variable; estas son las propiedades del sistema:\n " + props);
    }

    /**
     * Permite demostrar que en java las variables se pasan por valor excepto cuando es un objeto mutable
     */
    public void pasoVariable() {
        int entero = 128;
        System.out.println("Valor original  " + entero);
        cambio(entero);
        System.out.println("Valor cambio    " + entero);
        StringBuilder builder = new StringBuilder("Hola mundo");

        System.out.println("Valor original  " + builder);
        cambio(builder);
        System.out.println("Valor cambio    " + builder);

        System.out.println("Valor original  " + builder);
        cambioMutable(builder);
        System.out.println("Valor cambio    " + builder);
    }

    public static void cambio(int a) {
        a = 255;
    }

    public static void cambio(StringBuilder sb) {
        sb = new StringBuilder("Nuevo valor");
    }

    public static void cambioMutable(StringBuilder sb) {
        sb.append(": Modificacion por referencia(solo dentro de la misma instancia)");
    }
    
    /**
     * Error con numeros de punto flotante
     */
    public void puntoFlotante() {
        float numero1 = 0.6f;
        float numero2 = 0.5f;
        float res = numero2 - numero1;
        System.out.println("Resta Floats >>> " + res);

        double d1 = 0.4;
        double d2 = 0.5;
        double res2 = d2 - d1;
        System.out.println("Resta Doubles >>> " + res2);
    }

}
