/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.variables;

/**
 *
 * @author james
 */
public class Variables {
    
    private static final int CONSTANTE_ENTERA = 333;
    
    private int variableGlobalEntera;
    private static double variableEstatica;    
    public String variablePublicaString;
    
    static{
        
    }
    
    /**
     * Se puede usar para inicializar variables de instancia
     */
    public Variables() {
        this.variableGlobalEntera = CONSTANTE_ENTERA + Runtime.getRuntime().availableProcessors();
        this.variablePublicaString = "Mensaje inicializado";
    }
    
    public void imprimir(){  
        //En esta linea se incluye un cast de entero a short pues se sabe que el valor nunca sera mayor al maximo que soporta short
        short variableLocalShort = (short) Runtime.getRuntime().availableProcessors();
        /**
         * Unavariable estatica se puede modificar en cualqueir parte incluso desde fuera si el modificador de acceo lo permite 
         * sin embargo la inicializacion como tal se puede hacer en su declaracion o en el bloque static
        */
        variableEstatica = CONSTANTE_ENTERA * Math.PI;
        
        System.out.println("CONSTANTE_ENTERA: \t" + CONSTANTE_ENTERA);
        System.out.println("variableGlobalEntera: \t" + variableGlobalEntera);
        System.out.println("variableEstatica: \t" + variableEstatica);        
        System.out.println("variablePublicaString: \t" + variablePublicaString);
        System.out.println("variableLocalShort: \t" + variableLocalShort);
        
    }
    
    
}
