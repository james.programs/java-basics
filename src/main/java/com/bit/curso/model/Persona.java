/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author james
 */
public class Persona implements Serializable, Cloneable {
    private String nombres;
    private String apellidos;
    private String cedula;
    private Date fechaNacimiento;

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }
}
