/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso;

import com.bit.curso.acceso.ValidacionAcceso;
import com.bit.curso.annotations.PruebaAnotacion;
import com.bit.curso.annotations.PruebaReflection;
import com.bit.curso.collections.CollectionsTest;
import com.bit.curso.excepciones.CapturaExcepcion;
import com.bit.curso.excepciones.ExcepcionPueba;
import com.bit.curso.variables.PruebaTiposDato;
import com.bit.curso.variables.Variables;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author james
 */
public class Main {

    /**
     *
     */
    public static void conceptos() {
        PruebaTiposDato prueba = new PruebaTiposDato();
        System.out.println("======= CURSO DE JAVA TIPOS DE DATO ========");
        prueba.tipos();
        System.out.println("===================");
        prueba.pasoVariable();
        System.out.println("===================");
        prueba.puntoFlotante();
        System.out.println("======= CURSO DE JAVA VARIABLES ========");
        Variables variables = new Variables();
        variables.imprimir();
        System.out.println("======= CURSO DE JAVA MODIFICADORES DE ACCESO ========");
        ValidacionAcceso acceso = new ValidacionAcceso();
        //Modificacion mediante metodo set de variable privada, se recomienda siempre usar metodos get y set para acceder
        acceso.setVariablePrivada(333);
        //modificacion externa solo para fines educativos no se recomienda acceder directamente a la variable      
        acceso.variableVolatil = acceso.perimetroCirculo(10);
        acceso.imprimir();
    }

    public static void excepciones() {
        CapturaExcepcion oper = new CapturaExcepcion();
        oper.capturaExcepcion();
        System.out.println("================");
        oper.capturaFinally();
        System.out.println("================");
        oper.capturaDoble();
        System.out.println("================");
        oper.capturaMultiple();
        System.out.println("================");
        oper.capturaAnidada();
        System.out.println("================");
//        oper.lanzarExcepcion();
        System.out.println("================");
//        oper.relanzarExcepcion();
        System.out.println("================");
        try {
            oper.excepcionPersonalizada();
        } catch (ExcepcionPueba ex) {
            System.out.println("Que paso aqui?  " + ex);
            System.out.println("Superclase " + ex.getCause());
        }

    }

    public static void colecciones() {
//        System.out.println("Java Properties " + System.getProperties());
//        System.out.println("Java Prop linea <" + System.getProperty("file.separator") + ">");
        System.out.println("============= CURSO JAVA COLLECTIONS ===============");
        Properties p = new Properties();
        try {
            p.load(new FileInputStream("/data/local/capacitacion/demo.properties"));
        } catch (IOException e) {
            System.out.println("Error " + e);
        }
        System.out.println("Propiedades   " + p);
        System.out.println("===================");
        CollectionsTest col = new CollectionsTest();
        col.pruebaListas();
        System.out.println("========= ArrayList Vs LinkedList ==========");
        col.listaVS();
        System.out.println("===================");
        col.vectorStack();
        System.out.println("===================");
        col.testSet();
        System.out.println("===================");
        col.mapas();
    }
    
    public static void reflection(){
        PruebaReflection ref =  new PruebaReflection();
        ref.metadataSimple();
        System.out.println("============================");
        ref.operarReflection();
    } 
    
    public static void anotacion(){
        PruebaAnotacion anota =  new PruebaAnotacion();
//        ref.metadataSimple();
        anota.probarAnotacion();
    }

    public static void main(String[] args) {
        //        conceptos();
        //        excepciones();
        //        colecciones();
//                reflection();
        anotacion();
        
    }

}
