/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.acceso;

import java.math.BigDecimal;

/**
 *
 * @author james
 */
class ClaseDefault {
    
    protected  BigDecimal variableProtected;
    
    @Override
    public String toString() {
        return "Clase default visible solo en paquete";
    }
    
    
}
