/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.acceso;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 *
 * @author james
 */
public class ValidacionAcceso extends ClaseDefault {

    final static String CONSTANTE_STRING;
    final static StringBuilder CONSTANTE_STRING_BUILDER = new StringBuilder("String Builder");
    private int variablePrivada;
    int variableDefault;    
    public BigInteger variablePublica;
    public volatile double variableVolatil;
    
    static {
        System.out.println("Inicializacion en bloque static ");
        CONSTANTE_STRING = "String";
    }

    /**
     * Constructor, se inicializa las variables de punto fijo
     */
    public ValidacionAcceso() {
        this.variablePublica = new BigInteger("99");
        this.variableProtected =  new BigDecimal(Math.PI);  
        this.variableDefault = 111; 
    }

    public int getVariablePrivada() {
        return variablePrivada;
    }

    public void setVariablePrivada(int variablePrivada) {
        this.variablePrivada = variablePrivada;
    }
    
    /**
     * Metodo sincronizado y estricatmente en punto flotante 
     * @param a 
     * @return 
     */
    public synchronized strictfp double perimetroCirculo(double a) {
        return a * Math.PI;
    }
    
    /**
     * La declaracion nativa se realiza pero el metodo es similar a un asbtracto, el cuerpo o implementacion debera 
     * existir en un objeto nativo previamente cargado
     */
    public native void metodoNativo();
    

    public void imprimir() {
        System.out.println("Yo si puedo ver la clase default " + ClaseDefault.class.getName() + " => " + super.toString());
        System.out.println("========= Se puede modificar una constante? ==========");
        CONSTANTE_STRING.concat("\t AGREGO INFO");
        CONSTANTE_STRING.concat("\t AGREGO MAS INFO");
        System.out.println("CONSTANTE_STRING:  " + CONSTANTE_STRING);
        CONSTANTE_STRING_BUILDER.append("\t AGREGO INFO");
        CONSTANTE_STRING_BUILDER.append("\t AGREGO MAS INFO");
        System.out.println("CONSTANTE_STRING_BUILDER:  " + CONSTANTE_STRING_BUILDER);
        System.out.println("variablePrivada: \t" + variablePrivada);
        System.out.println("variableDefault: \t" + variableDefault);
        System.out.println("variableProtected: \t" + variableProtected);
        System.out.println("variablePublica: \t" + variablePublica);
        System.out.println("variableVolatil: \t" + variableVolatil);
    }
        
}
