/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.interfaces;

/**
 *
 * @author james
 */
public abstract class ClaseAbstracta {
    
    public void metodo(){
        System.out.println("");
    }
    
    public abstract double calculoIva(double a);
    
}
