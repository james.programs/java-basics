/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.interfaces;

/**
 *
 * @author james
 */
public interface CalculoTotal {

    
    private double calcularPerimetro(double a){
        return a * 3.141592654;
    }
    
    double calculoTotal(double a);

    double calculoDos(double a);

}
