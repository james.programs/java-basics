/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.collections;

import java.util.*;
import java.util.function.Predicate;

/**
 *
 * @author james
 */
public class CollectionsTest {

    List lista;
    List<String> lista2;
    Vector vector;
    Stack stack;
    Set set;
    Map<Object, String> map;

    public void pruebaListas() {
        lista = new ArrayList();
        lista.add(1.2);
        lista.add("cadena");
        lista.add(new Date());
        System.out.println("lista  " + lista);
        System.out.println("Set valor  ");
        lista.set(2, Math.PI);
        System.out.println("lista  " + lista);
        lista.remove(1.2);
        System.out.println("Elimino?   " + lista);
        lista.add(new Date());
        lista.remove(0);

        System.out.println("Elimino? " + lista);
        lista2 = new ArrayList<>();
        lista2.add("cadena");
        lista2.add("ejemplo");
        lista2.add("algo mas");
        System.out.println("Lista  " + lista2);

        lista.addAll(lista2);
        System.out.println("Lista unificada " + lista);
        System.out.println("=================");
        for (Object o : lista) {
            System.out.println("Element " + o);
        }
        System.out.println("=================");
        lista.forEach(o -> {
            System.out.println("Element " + o);
        });
        System.out.println("=================");
        lista.removeIf(o -> {
            return o instanceof Date;
        });
        lista.removeIf(new Predicate() {
            @Override
            public boolean test(Object o) {
                return o instanceof Date;
            }
        });

        System.out.println("Lista removeIf " + lista);
        lista.sort((o1, o2) -> {
            return o1.hashCode();
        });
        System.out.println("Lista ordenada " + lista);
        lista.removeAll(lista2);
        System.out.println("Lista reducida " + lista);
        System.out.println("=================");
        lista = new LinkedList();
        lista.add("ABCDE");
        lista.add("EJEMPLO DOS");
        lista.add("COMPLETAR DATOS");
        lista.add("FOMRULARIO DE INGRESO");
        System.out.println("Linked list  " + lista);
    }

    public void listaVS() {
        List<Double> arrayL = new ArrayList<>();
        List<Double> linkL = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            double r = i-5;
            arrayL.add(r);
            linkL.add(r);
        }
        arrayL.remove(0.0);
        linkL.remove(0.0); 
        arrayL.add(3.1);
        linkL.add(3.1);
        linkL.remove(0);
        arrayL.remove(0);
        linkL.remove(0);
        arrayL.remove(0);
        System.out.println("ArrayList:  \t" + arrayL);
        System.out.println("LinkedLIst: \t" + linkL);
    }

    public void vectorStack() {
        vector = new Vector(1);
        vector.add("ABCDE");
        vector.add("EJEMPLO DOS");
        vector.add("COMPLETAR DATOS");
        vector.add("FOMRULARIO DE INGRESO");
        System.out.println("Vector   " + vector);
        System.out.println("Informacion  " + vector.size() + " " + vector.isEmpty() + " " + vector.contains("ABCDE") + " " + vector.contains("ABCDEH"));
        stack = new Stack();
        stack.add("ELEMENTO PILA 1");
        stack.add("ELEMENTO PILA 2");
        stack.add("ELEMENTO PILA 3");
        stack.add("ELEMENTO PILA 4");
        System.out.println("Pila   " + stack);
        System.out.println("Pila peek  " + stack.peek());
        System.out.println("Pila pop   " + stack.pop());
        stack.push("ELEMENTO PILA 5");
        System.out.println("Pila   " + stack);
        System.out.println("Pila peek  " + stack.peek());
        System.out.println("Valor indice 2  " + stack.get(2));
        stack.remove(2);
        System.out.println("Pila   " + stack);
    }

    public void testSet() {
        set = new HashSet<>();
        set.add(5);
        set.add(2);
        set.add(3);
        set.add(2);
        set.add(6);
        set.add(-5);
        set.add(0);
        set.add(2);
        System.out.println("HashSet " + set);
        set = new TreeSet();
        set.add(5);
        set.add(2);
        set.add(3);
        set.add(2);
        set.add(6);
        set.add(-5);
        set.add(0);
        set.add(2);
        System.out.println("TreeSet " + set);
        set = new LinkedHashSet();
        set.add(5);
        set.add(2);
        set.add(3);
        set.add(2);
        set.add(6);
        set.add(-5);
        set.add(0);
        set.add(2);
        System.out.println("LinkedHashSet " + set);
        System.out.println("Informacion  " + set.size() + " " + set.isEmpty() + " " + set.contains(6) + " " + set.contains(-6));
        set.forEach(o -> {
            System.out.println("Element " + o);
        });
    }

    public void mapas() {
        map = new HashMap<>();
        map.put(3, "Valor uno");
        map.put(1, "" + Math.PI);
        map.put(2, "" + new Date());
        map.put(-2, "" + Runtime.getRuntime().freeMemory());
        map.put(3, "Un cambio");
        System.out.println("HashMap " + map);
        //ordenar un mapa con un stream
        System.out.println("Mapa ordenado  ");
        map.entrySet()
                .stream()
                .sorted(Map.Entry.<Object, String>comparingByValue())
                .forEach(System.out::println);

        map = new Hashtable();
        map.put(3, "Valor uno");
        map.put(1, "" + Math.PI);
        map.put(2, "" + new Date());
        map.put(-2, "" + Runtime.getRuntime().freeMemory());
        map.put(3, "Un cambio");
        System.out.println("HashTable " + map);
        map = new TreeMap<>();
        map.put(3, "Valor uno");
        map.put(1, "" + Math.PI);
        map.put(2, "" + new Date());
        map.put(-2, "" + Runtime.getRuntime().freeMemory());
        map.put(3, "Un cambio");
        System.out.println("TreeMap " + map);
        map = new LinkedHashMap<>();
        map.put(3, "0");
        map.put(1, "" + Math.PI);
        map.put(2, "" + new Date());
        map.put(-2, "" + Runtime.getRuntime().freeMemory());
        if (map.containsKey(3)) {
            System.out.println("Ya existe la clave 3 no se modificara");
        } else {
            map.put(3, "100");
        }
        System.out.println("LinkedHashMap " + map);
        System.out.println("Get " + map.get(2));
        System.out.println("Informacion " + map.size() + "  " + map.isEmpty() + "  " + map.containsKey(2) + "  " + map.containsValue("" + Math.PI));
        for (Map.Entry e : map.entrySet()) {
            System.out.println("Entry   " + e.getKey() + ": " + e.getValue());
        }
        System.out.println("=====================");
        for (Object o : map.keySet()) {
            System.out.println("Key   " + o);
        }
        System.out.println("=====================");
        for (String v : map.values()) {
            System.out.println("Value   " + v);
        }

    }

}
