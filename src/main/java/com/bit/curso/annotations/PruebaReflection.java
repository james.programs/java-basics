/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.annotations;

import com.bit.curso.model.Persona;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author james
 */
public class PruebaReflection {

    private Persona persona;

    public PruebaReflection() {
        persona = new Persona();
    }

    public void metadataSimple() {
        System.out.println("Metadata basica desde el class");
        System.out.println("> " + Persona.class.getName());
        System.out.println("> " + Persona.class.getCanonicalName());
        System.out.println("> " + Persona.class.getPackageName());
        System.out.println("> " + Persona.class.getSimpleName());
        System.out.println("> " + Persona.class.getTypeName());
        System.out.println("=====================");
        System.out.println("> " + ArrayList.class.getSuperclass());
        for (Class c : Persona.class.getInterfaces()) {
            System.out.println("\t> " + c.getName());
        }
        System.out.println("=====================");
        for (Class c : ArrayList.class.getDeclaredClasses()) {
            System.out.println("\t> " + c.getName());
        }
        System.out.println("=====================");
        for (Class c : ArrayList.class.getNestMembers()) {
            System.out.println("\t> " + c.getName());
        }
        System.out.println("========== PERSONA VARIABLES ===========");
        for (Field f : Persona.class.getFields()) {
            System.out.println("\t> " + f.getName());
        }
        System.out.println("========== PERSONA VARIABLES ===========");
        for (Field f : Persona.class.getDeclaredFields()) {
            System.out.println("\t> " + f.getName() + "\t  " + f.getType());
        }

    }

    public void operarReflection() {
        for (Field field : Persona.class.getDeclaredFields()) {
            System.out.println("\t>variable: " + field.getType() + ": " + field.getName());
            try {
                field.setAccessible(Boolean.TRUE);
                Object valor = field.get(persona);
                System.out.println("\tValor: " + valor);
                if (valor == null && field.getType().equals(String.class)) {
                    valor = "Ejemplo de set valor";
                    field.set(persona, valor);
                    System.out.println("\tSet?: " + valor);
                }
                field.setAccessible(Boolean.FALSE);
            } catch (IllegalArgumentException ex) {
                System.out.println("Argumento Ilegal  " + ex);
            } catch (IllegalAccessException ex) {
                System.out.println("Acceso Ilegal  " + ex);
            }

        }

        System.out.println("Funciono?  " + persona.getCedula() + "  " + persona.getNombres());
    }

}
