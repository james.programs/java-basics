/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.annotations;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author james
 */
public class Procesar {

    public void procesa(PruebaAnotacion objeto) {
        try {
            for (Field field : objeto.getClass().getDeclaredFields()) {
                ejecutaAnotacion(field, objeto);
            }
        } catch (Exception e) {
            System.err.println("Que paso aqui?  " + e);
        }
    }

    private void ejecutaAnotacion(Field field, PruebaAnotacion prueba)
            throws IllegalAccessException, IllegalArgumentException {
        final AnotacionSimple anotacion = field.getAnnotation(AnotacionSimple.class);
        System.out.println("Tengo anotacion?  " + field.getName() + " => " + anotacion);
        if (anotacion != null) {
            Boolean est = field.canAccess(prueba);
            field.setAccessible(Boolean.TRUE);
            Object valor = field.get(prueba);
            if (valor == null) {
                valor = anotacion.identificador();
                field.set(prueba, valor);
            }
            field.setAccessible(est);
        }
    }

    public void procesaEjecucion(PruebaAnotacion objeto) {
        try {
            for (Method method : objeto.getClass().getMethods()) {
                ejecutaMetodo(method, objeto);
            }
        } catch (Exception e) {
            System.err.println("Que paso aqui?  " + e);
        }
    }

    public void ejecutaMetodo(Method method, PruebaAnotacion prueba)
            throws IllegalAccessException, IllegalArgumentException,ArithmeticException, InvocationTargetException{
        AnotacionEjecuta anotacion = method.getAnnotation(AnotacionEjecuta.class);
        if(anotacion != null){
            System.out.println("Tengo anotacion  " + method.getName() + " => " + anotacion);
            int max = anotacion.ejecuciones();
            for(int i = 0; i < max; i++){
                if(method.getParameterCount() > 0) {
                    method.invoke(prueba, "" + i);
                }else{
                    method.invoke(prueba);
                }
            }
        }
    }


}
