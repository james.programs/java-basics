/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bit.curso.annotations;

/**
 *
 * @author james
 */
public class PruebaAnotacion {
 
    @AnotacionSimple(identificador = "Juan Carlos")
    private String nombre;
    @AnotacionSimple
    private String cedula;
    @AnotacionSimple
    private String  ciudad;

    
    public void probarAnotacion(){
        final PruebaAnotacion test = new PruebaAnotacion();
        test.setCedula("1712345678");

        System.out.println("Valores?  " + test.nombre + "  " + test.cedula + "  " + test.ciudad);
        final Procesar procesa = new Procesar();
        procesa.procesa(test);
        System.out.println("Se establecio?  " + test.nombre + "  " + test.cedula + "  " + test.ciudad);
        System.out.println("============= Anotacion 2 ===========");
        procesa.procesaEjecucion(test);
    }


    @AnotacionEjecuta
    public static void imprime (){
        System.out.println("Ok se ejecuto en " + System.currentTimeMillis());
    }

    @AnotacionEjecuta(ejecuciones = 5)
    public static void imprimeMas (String eje){
        System.out.println("Ejecucion multiple? " + eje);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
}
